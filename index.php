<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>weh school</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="css/lightslider.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	
	<?php include 'inc/header-wrapper.php' ?>
	<?php include 'inc/content-wrapper.php' ?>
	<?php include 'inc/footer-wrapper.php' ?>

	<script src="js/jquery-3.1.1.js" type="text/javascript"></script>
	<script src="js/bootstrap.js" type="text/javascript"></script>
	<script src="js/lightslider.js" type="text/javascript"></script>
	<script src="js/script.js" type="text/javascript"></script>

</body>
</html>