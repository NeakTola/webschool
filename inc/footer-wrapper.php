<div class="footer-wrapper">
	<div class="footer-content">
		<div class="container">
			<div class="footer-columns row">
				<div class="col-md-4">
					<div class="widget">
						<div class="widget-title">
							<h2>Recent Posts</h2>
						</div>
						<div class="widget-content">
							<ul class="recent-posts">
								<li class="post">
									<div class="post-thumb">
										<a href="post-single.html">
											<img class="lazy" src="img/4.jpg" alt="Post Title Placeholder" style="opacity: 1;">
										</a>
									</div><!-- Post Image -->
									<div class="post-content">
										<h6 class="post-title">
											<a href="post-single.html">This Theory Of Why Starbucks Your Name Is Pretty Smart</a>
										</h6><!-- Post Title -->
										<ul class="post-meta">
											<li><i class="fa fa-clock-o"></i> October 30, 2016</li>
										</ul><!-- Post Meta -->
									</div><!-- Post Content -->
								</li><!-- Post -->
								<li class="post">
									<div class="post-thumb">
										<a href="post-single.html">
											<img class="lazy" src="img/5.jpg" alt="Post Title Placeholder" style="opacity: 1;">
										</a>
									</div><!-- Post Image -->
									<div class="post-content">
										<h6 class="post-title">
											<a href="post-single.html">Do You Know What Happened In Tech The First Week</a>
										</h6><!-- Post Title -->
										<ul class="post-meta">
											<li><i class="fa fa-clock-o"></i> October 30, 2016</li>
										</ul><!-- Post Meta -->
									</div><!-- Post Content -->
								</li><!-- Post -->
								<li class="post">
									<div class="post-thumb">
										<a href="post-single.html">
											<img class="lazy" src="img/12.jpg" alt="Post Title Placeholder" style="opacity: 1;">
										</a>
									</div><!-- Post Image -->
									<div class="post-content">
										<h6 class="post-title">
											<a href="post-single.html">This Theory Of Why Starbucks Your Name Is Pretty Smart</a>
										</h6><!-- Post Title -->
										<ul class="post-meta">
											<li><i class="fa fa-clock-o"></i> October 30, 2016</li>
										</ul><!-- Post Meta -->
									</div><!-- Post Content -->
								</li><!-- Post -->
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="widget">
						<div class="widget-title">
							<h2>Categories</h2>
						</div>
						<div class="widget-content list-label-widget-content">
							<ul>
								<li>
									<a dir="ltr" href="page-category.html">Fashion</a>
									<span dir="ltr">(4)</span>
								</li>
								<li>
									<a dir="ltr" href="page-category.html">Girls</a>
									<span dir="ltr">(4)</span>
								</li>
								<li>
									<a dir="ltr" href="page-category.html">Life Style</a>
									<span dir="ltr">(4)</span>
								</li>
								<li>
									<a dir="ltr" href="page-category.html">Music</a>
									<span dir="ltr">(2)</span>
								</li>
								<li>
									<a dir="ltr" href="page-category.html">Travel</a>
									<span dir="ltr">(9)</span>
								</li>
								<li>
									<a dir="ltr" href="page-category.html">Technology</a>
									<span dir="ltr">(4)</span>
								</li>
								<li>
									<a dir="ltr" href="page-category.html">Blogging</a>
									<span dir="ltr">(7)</span>
								</li>
								<li>
									<a dir="ltr" href="page-category.html">Personal</a>
									<span dir="ltr">(9)</span>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="widget">
						<div class="widget-title">
							<h2>@instagram</h2>
						</div>
						<div class="widget-content">
							<div class="instagram-feeds row-gallery">
								<a href="#" class="col-sm-3 col-xs-3"><img src="img/gallery/8.jpg" alt="Instagram Image"></a>
								<a href="#" class="col-sm-3 col-xs-3"><img src="img/gallery/7.jpg" alt="Instagram Image"></a>
								<a href="#" class="col-sm-3 col-xs-3"><img src="img/gallery/3.jpg" alt="Instagram Image"></a>
								<a href="#" class="col-sm-3 col-xs-3"><img src="img/gallery/4.jpg" alt="Instagram Image"></a>
								<a href="#" class="col-sm-3 col-xs-3"><img src="img/gallery/5.jpg" alt="Instagram Image"></a>
								<a href="#" class="col-sm-3 col-xs-3"><img src="img/gallery/6.jpg" alt="Instagram Image"></a>
								<a href="#" class="col-sm-3 col-xs-3"><img src="img/gallery/1.jpg" alt="Instagram Image"></a>
								<a href="#" class="col-sm-3 col-xs-3"><img src="img/gallery/2.jpg" alt="Instagram Image"></a>
							</div>
						</div>
					</div>
					<div class="widget FollowByEmail">
						<div class="widget-title">
							<h2>Follow By Email</h2>
						</div>
						<div class="widget-content">
							<div class="follow-by-email-inner">
								<form action="">
									<table>
										<tbody>
										<tr>
											<td>
												<input class="follow-by-email-address" name="email" placeholder="Your Email Adress..." type="text">
											</td>
											<td>
												<input class="follow-by-email-submit" type="submit" value="Subscribe">
											</td>
										</tr>
										</tbody>
									</table>
									<input name="uri" type="hidden" value="TheStyle-LifeStyleBloggerTemplate">
									<input name="loc" type="hidden" value="en_US">
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-columns row">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="widget Text">
						<div class="widget-content about-widget">
							<div><a href="http://localhost/web-school/"><img src="img/logo_dark.png" alt="Logo Footer"></a></div>
							<p>Is an elegant &amp; clean News Editorial &amp; Magazine Blogger Theme, comes with 3 predefined homepage variations and a lot of features needed in any blogger template.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<div class="widget">
						<div class="widget-title">
							<h2>Connect With Us</h2>
						</div>
						<div class="widget-content social-icons">
							<a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
							<a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
							<a class="instagram" href="#"><i class="fa fa-instagram"></i></a>
							<a class="google-plus" href="#"><i class="fa fa-google-plus"></i></a>
							<a class="snapchat-ghost" href="#"><i class="fa fa-snapchat-ghost"></i></a>
							<a class="dribbble" href="#"><i class="fa fa-dribbble"></i></a>
							<a class="vimeo" href="#"><i class="fa fa-vimeo"></i></a>
							<a class="behance" href="#"><i class="fa fa-behance"></i></a>
							<a class="rss" href="#"><i class="fa fa-rss"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="copyright-wrapper">
		<div class="copyright-content container">
			<div class="copyright-left">
				The theme being used in this demo is CleverCourse
			</div>
			<div class="copyright-right">
				Copyright © 2015 - All Right Reserved
			</div>
		</div>
		<div class="totop" style="display: block;"><i class="fa fa-angle-up"></i></div>
	</div>
</div>