<section class="featured-posts">
	<div class="container">
		<div class="row row-gallery">
			<div class="first col-md-7 col-sm-12">
				<div class="post has-caption" style="background-image: url(http://localhost/web-School/img/1.jpg);">
					<div class="post-content">
						<div class="post-category">
							<a href="#">Travel</a>
						</div>
						<h2 class="post-title">
							<a href="#">Travel Women Is Having Horrible No Good, Very Bad Day</a>
						</h2>
						<ul class="post-meta">
							<li>By <strong>Infinyteam</strong></li>
							<li><i class="fa fa-clock-o"></i> 13 October, 2017</li>
							<li><i class="fa fa-comment-o"></i> 250</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-5 col-sm-6">
				<div class="post has-caption" style="background-image: url(http://localhost/web-School/img/2.jpg);">
					<div class="post-content">
						<div class="post-category">
							<a href="#">Fashion</a>
						</div>
						<h5 class="post-title">
							<a href="#">Here How Samsung Is Forcing People To Turn In Their Galaxy Note7 Phones</a>
						</h5>
					</div>
				</div>
			</div>
			<div class="col-md-5 col-sm-6">
				<div class="post has-caption" style="background-image: url(http://localhost/web-School/img/3.jpg);">
					<div class="post-content">
						<div class="post-category">
							<a href="#">Life Style</a>
						</div>
						<h5 class="post-title">
							<a href="#">Seven Features Of Travel That Make Everyone Love It</a>
						</h5>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End featured-posts -->
<section class="trending-post">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="blog-layout">
					<div class="blog-title">
						<h2>TRENDING POSTS</h2>
					</div>
					<div class="blog-wrapper">
						<div class="demo">
					        <div class="item">
					            <ul id="content-slider" class="content-slider">
					                <li>
					                	<div class="post">
					                		<div class="post-thumb">
					                    		<a href="#"><img src="http://localhost/web-School/img/10.jpg" alt=""></a>
						                    </div>
						                    <div class="post-content">
						                    	<h6 class="post-title"><a href="#">And love with you sometimes I feel good</a></h6>
						                    </div>
					                	</div>
					                </li>
					                <li>
					                	<div class="post">
					                		<div class="post-thumb">
					                    		<a href="#"><img src="http://localhost/web-School/img/10.jpg" alt=""></a>
						                    </div>
						                    <div class="post-content">
						                    	<h6 class="post-title"><a href="#">And love with you sometimes I feel good</a></h6>
						                    </div>
					                	</div>
					                </li>
					                <li>
					                	<div class="post">
					                		<div class="post-thumb">
					                    		<a href="#"><img src="http://localhost/web-School/img/10.jpg" alt=""></a>
						                    </div>
						                    <div class="post-content">
						                    	<h6 class="post-title"><a href="#">And love with you sometimes I feel good</a></h6>
						                    </div>
					                	</div>
					                </li>
					                <li>
					                	<div class="post">
					                		<div class="post-thumb">
					                    		<a href="#"><img src="http://localhost/web-School/img/10.jpg" alt=""></a>
						                    </div>
						                    <div class="post-content">
						                    	<h6 class="post-title"><a href="#">And love with you sometimes I feel good</a></h6>
						                    </div>
					                	</div>
					                </li>
					                <li>
					                	<div class="post">
					                		<div class="post-thumb">
					                    		<a href="#"><img src="http://localhost/web-School/img/10.jpg" alt=""></a>
						                    </div>
						                    <div class="post-content">
						                    	<h6 class="post-title"><a href="#">And love with you sometimes I feel good</a></h6>
						                    </div>
					                	</div>
					                </li>
					                <li>
					                	<div class="post">
					                		<div class="post-thumb">
					                    		<a href="#"><img src="http://localhost/web-School/img/10.jpg" alt=""></a>
						                    </div>
						                    <div class="post-content">
						                    	<h6 class="post-title"><a href="#">And love with you sometimes I feel good</a></h6>
						                    </div>
					                	</div>
					                </li>
					                <li>
					                	<div class="post">
					                		<div class="post-thumb">
					                    		<a href="#"><img src="http://localhost/web-School/img/10.jpg" alt=""></a>
						                    </div>
						                    <div class="post-content">
						                    	<h6 class="post-title"><a href="#">And love with you sometimes I feel good</a></h6>
						                    </div>
					                	</div>
					                </li>
					                <li>
					                	<div class="post">
					                		<div class="post-thumb">
					                    		<a href="#"><img src="http://localhost/web-School/img/10.jpg" alt=""></a>
						                    </div>
						                    <div class="post-content">
						                    	<h6 class="post-title"><a href="#">And love with you sometimes I feel good</a></h6>
						                    </div>
					                	</div>
					                </li>
					                <li>
					                	<div class="post">
					                		<div class="post-thumb">
					                    		<a href="#"><img src="http://localhost/web-School/img/10.jpg" alt=""></a>
						                    </div>
						                    <div class="post-content">
						                    	<h6 class="post-title"><a href="#">And love with you sometimes I feel good</a></h6>
						                    </div>
					                	</div>
					                </li>
					                <li>
					                	<div class="post">
					                		<div class="post-thumb">
					                    		<a href="#"><img src="http://localhost/web-School/img/10.jpg" alt=""></a>
						                    </div>
						                    <div class="post-content">
						                    	<h6 class="post-title"><a href="#">And love with you sometimes I feel good</a></h6>
						                    </div>
					                	</div>
					                </li>
					            </ul>
					        </div>
					    </div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</section>
<!-- End trending-post -->
<section class="section-area">
	<div class="container">
		<div class="row">
			<div class="main col-md-8">
				<div class="blog-layout layout-1">
					<div class="blog-title">
						<h2>Hot News</h2>
					</div>
					<!-- End blog-title -->
					<div class="blog-wrapper row">
						<div class="col-sm-8">
							<article class="post first">
								<div class="post-thumb">
									<a href="page.php"><img src="http://localhost/web-School/img/3.jpg" alt=""></a>
								</div>
								<!-- End post-thumb -->
								<div class="post-content">
									<h4 class="post-title">
										<a href="page.php">Pick A Holiday Dessert Tell You Which Christmas Movie</a>
									</h4>
									<p class="post_entry">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus tenetur quisquam ut, quas praesentium dolores iure ullam molestias aperiam vero, saepe inventore est veniam ipsum...
									</p>
									<ul class="post-meta">
										<li><i class="fa fa-clock-o"></i> October 16, 2017</li>
										<li><i class="fa fa-comment-o"></i> 240</li>
									</ul>
								</div>
								<!-- End post-content -->
							</article>
						</div>
						<!-- End Column Post -->
						<div class="col-sm-4">
							<article class="post">
								<div class="post-thumb">
									<a href="#"><img src="http://localhost/demo-School/img/2.jpg" alt=""></a>
								</div>
								<div class="post-content">
									<h6 class="post-title">
										<a href="#">This Theory Of Why Starbucks Your Name Is Pretty Smart</a>
									</h6>
									<ul class="post-meta">
										<li><i class="fa fa-clock-o"></i> October 30, 2016</li>
									</ul>
								</div>
							</article>
							<!-- End post -->
							<article class="post">
								<div class="post-thumb">
									<a href="#"><img src="http://localhost/demo-School/img/1.jpg" alt=""></a>
								</div>
								<div class="post-content">
									<h6 class="post-title">
										<a href="#">Do You Know What Happened In Tech The First Week</a>
									</h6>
									<ul class="post-meta">
										<li><i class="fa fa-clock-o"></i> October 30, 2016</li>
									</ul>
								</div>
							</article>
						</div>
						<!-- End Column Post -->
					</div>
				</div>
				<!-- End blog-layout -->
				<div class="blog-layout layout-2 tiny-thumb">
					<div class="blog-title">
						<h2>FASHION</h2>
					</div>
					<!-- End blog-title -->
					<div class="blog-wrapper row">
						<div class="col-sm-6">
							<article class="post first">
								<div class="post-thumb">
									<a href="#"><img src="http://localhost/demo-School/img/2.jpg" alt=""></a>
								</div>
								<!-- End post-thumb -->
								<div class="post-content">
									<div class="post-content">
										<h4 class="post-title">
											<a href="#">Pick A Holiday Dessert Tell You Which Christmas Movie</a>
										</h4>
										<p class="post_entry">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus tenetur quisquam ut, quas praesentium dolores iure ullam molestias aperiam vero, saepe inventore est veniam ipsum...
										</p>
										<ul class="post-meta">
											<li><i class="fa fa-clock-o"></i> October 16, 2017</li>
											<li><i class="fa fa-comment-o"></i> 240</li>
										</ul>
									</div>
								</div>
								<!-- End post-content -->
							</article>
						</div>
						<!-- End Column -->
						<div class="col-sm-6">
							<article class="post">
								<div class="post-thumb">
									<a href="#"><img src="http://localhost/demo-School/img/12.jpg" alt=""></a>
								</div>
								<div class="post-content">
									<h6 class="post-title">
										<a href="#">Do You Know What Happened In Tech The First Week</a>
									</h6>
									<ul class="post-meta">
										<li><i class="fa fa-clock-o"></i> October 30, 2016</li>
									</ul>
								</div>
							</article>
							<!-- End post -->
							<article class="post">
								<div class="post-thumb">
									<a href="#"><img src="http://localhost/demo-School/img/12.jpg" alt=""></a>
								</div>
								<div class="post-content">
									<h6 class="post-title">
										<a href="#">Do You Know What Happened In Tech The First Week</a>
									</h6>
									<ul class="post-meta">
										<li><i class="fa fa-clock-o"></i> October 30, 2016</li>
									</ul>
								</div>
							</article>
							<!-- End post -->
							<article class="post">
								<div class="post-thumb">
									<a href="#"><img src="http://localhost/demo-School/img/12.jpg" alt=""></a>
								</div>
								<div class="post-content">
									<h6 class="post-title">
										<a href="#">Do You Know What Happened In Tech The First Week</a>
									</h6>
									<ul class="post-meta">
										<li><i class="fa fa-clock-o"></i> October 30, 2016</li>
									</ul>
								</div>
							</article>
							<!-- End post -->
							<article class="post">
								<div class="post-thumb">
									<a href="#"><img src="http://localhost/demo-School/img/12.jpg" alt=""></a>
								</div>
								<div class="post-content">
									<h6 class="post-title">
										<a href="#">Do You Know What Happened In Tech The First Week</a>
									</h6>
									<ul class="post-meta">
										<li><i class="fa fa-clock-o"></i> October 30, 2016</li>
									</ul>
								</div>
							</article>
						</div>
					</div>
				</div>
				<!-- End blog-layout -->
				<div class="blog-layout layout-3">
					<div class="blog-title">
						<h2>People</h2>
					</div>
					<!-- End blog-title -->
					<div class="blog-wrapper row">
						<div class="col-sm-12">
							<article class="post first">
								<div class="post-thumb">
									<a href="#"><img src="http://localhost/web-school/img/17.jpg" alt=""></a>
								</div>
								<!-- End post-thumb -->
								<div class="post-content">
									<h4 class="post-title">
										<a href="post-single.html">Pick A Holiday Dessert Tell You Which Christmas Movie</a>
									</h4>
									<!-- End post-title -->
									<p class="post-entry">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus tenetur quisquam ut, quas praesentium dolores iure ullam molestias aperiam vero, saepe inventore est veniam ipsum...
									</p>
									<!-- End post-entry -->
									<ul class="post-meta">
										<li><i class="fa fa-clock-o"></i> October 16, 2017</li>
										<li><i class="fa fa-comment-o"></i> 240</li>
									</ul>
									<!-- End post-meta -->
								</div>
								<!-- End post content -->
							</article>
						</div>
						<!-- End Column -->
						<div class="col-sm-12">
							<article class="post">
								<div class="post-thumb">
									<a href="#">
										<img class="lazy" src="http://localhost/web-school/img/18.jpg" alt="#" style="opacity: 1;">
									</a>
								</div><!-- Post Image -->
								<div class="post-content">
									<h6 class="post-title">
										<a href="#">This Theory Of Why Starbucks Your Name Is Pretty Smart</a>
									</h6><!-- Post Title -->
									<ul class="post-meta">
										<li><i class="fa fa-clock-o"></i> October 30, 2016</li>
									</ul><!-- Post Meta -->
								</div><!-- Post Content -->
							</article>
							<!-- End post -->
							<article class="post">
								<div class="post-thumb">
									<a href="#">
										<img class="lazy" src="http://localhost/web-school/img/19.jpg" style="opacity: 1;">
									</a>
								</div><!-- Post Image -->
								<div class="post-content">
									<h6 class="post-title">
										<a href="#">Do You Know What Happened In Tech The First Week</a>
									</h6><!-- Post Title -->
									<ul class="post-meta">
										<li><i class="fa fa-clock-o"></i> October 30, 2016</li>
									</ul><!-- Post Meta -->
								</div><!-- Post Content -->
							</article>
							<!-- End post -->
							<article class="post">
								<div class="post-thumb">
									<a href="post-single.html">
										<img class="lazy" src="http://localhost/web-school/img/20.jpg" style="opacity: 1;">
									</a>
								</div><!-- Post Image -->
								<div class="post-content">
									<h6 class="post-title">
										<a href="#">This Theory Of Why Starbucks Your Name Is Pretty Smart</a>
									</h6><!-- Post Title -->
									<ul class="post-meta">
										<li><i class="fa fa-clock-o"></i> October 30, 2016</li>
									</ul><!-- Post Meta -->
								</div><!-- Post Content -->
							</article>
							<!-- End post -->
							<article class="post">
								<div class="post-thumb">
									<a href="#">
										<img class="lazy" src="http://localhost/web-school/img/21.jpg" style="opacity: 1;">
									</a>
								</div><!-- Post Image -->
								<div class="post-content">
									<h6 class="post-title">
										<a href="post-single.html">Do You Know What Happened In Tech The First Week</a>
									</h6><!-- Post Title -->
									<ul class="post-meta">
										<li><i class="fa fa-clock-o"></i> October 30, 2016</li>
									</ul><!-- Post Meta -->
								</div><!-- Post Content -->
							</article>
							<!-- End post -->
						</div>
					</div>
				</div>
			</div>
			<!-- End main -->
			<div class="sidebar col-md-4" style="position: relative;overflow: visible;box-sizing: border-box;min-height: 1px;">
				<div class="sidebar-widget" style="padding-top: 0px; padding-bottom: 1px; position: static;">
					<div class="widget">
						<div class="widget-title">
							<h2>Follow Us</h2>
						</div>
						<!-- End widget-title -->
						<div class="widget-content social-counter">
							<a href="#" class="facebook">
								<i class="fa fa-facebook"></i>
								<span class="count">1.42k</span>
							</a>
							<a href="#" class="twitter">
								<i class="fa fa-twitter"></i>
								<span class="count">1.42k</span>
							</a>
							<a href="#" class="gplus">
								<i class="fa fa-google-plus"></i>
								<span class="count">1.42k</span>
							</a>
							<a href="#" class="instagram">
								<i class="fa fa-instagram"></i>
								<span class="count">1.42k</span>
							</a>
							<a href="#" class="dribbble">
								<i class="fa fa-dribbble"></i>
								<span class="count">1.42k</span>
							</a>
							<a href="#" class="snapchat">
								<i class="fa fa-snapchat-ghost"></i>
								<span class="count">1.42k</span>
							</a>
						</div>
						<!-- End widet-content -->
					</div>
					<!-- End widget -->
					<div class="widget">
						<div class="widget-title">
							<h2>Recent Posts</h2>
						</div>
						<div class="widget-centent">
							<ul class="recent-posts">
								<li class="post">
									<div class="post-thumb">
										<a href="#">
											<img src="http://localhost/demo-School/img/4.jpg" alt="" style="opacity: 1;">
										</a>
									</div>
									<div class="post-content">
										<h6 class="post-title">
											<a href="#">This Theory Of Why Starbucks Your Name Is Pretty Smart</a>
										</h6>
										<ul class="post-meta">
											<li><i class="fa fa-clock-o"></i> October 30, 2016</li>
										</ul>
									</div>
								</li>
								<li class="post">
									<div class="post-thumb">
										<a href="#">
											<img src="http://localhost/demo-School/img/5.jpg" alt="" style="opacity: 1;">
										</a>
									</div>
									<div class="post-content">
										<h6 class="post-title">
											<a href="#">Do You Know What Happened In Tech The </a>
										</h6>
										<ul class="post-meta">
											<li><i class="fa fa-clock-o"></i> October 30, 2016</li>
										</ul>
									</div>
								</li>
								<li class="post">
									<div class="post-thumb">
										<a href="#">
											<img src="http://localhost/demo-School/img/12.jpg" alt="" style="opacity: 1;">
										</a>
									</div>
									<div class="post-content">
										<h6 class="post-title">
											<a href="#">This Theory Of Why Starbucks Your Name Is </a>
										</h6>
										<ul class="post-meta">
											<li><i class="fa fa-clock-o"></i> October 30, 2016</li>
										</ul>
									</div>
								</li>
								<li class="post">
									<div class="post-thumb">
										<a href="#">
											<img src="http://localhost/demo-School/img/12.jpg" alt="" style="opacity: 1;">
										</a>
									</div>
									<div class="post-content">
										<h6 class="post-title">
											<a href="#">This Theory Of Why Starbucks Your Name Is </a>
										</h6>
										<ul class="post-meta">
											<li><i class="fa fa-clock-o"></i> October 30, 2016</li>
										</ul>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!-- End sidebar -->
		</div>
	</div>
</section>
