<header class="header-wrapper">
	<!-- top-navigation -->
	<div class="top-navigation-wrapper">
		<div class="top-navigation-container container">
            <div class="row">
    			<div class="top-navigation-left">
    				<div class="top-navigation-text">
    					The theme being used in this demo is <a href="#" target="_blank">CleverCourse</a>
    				</div>
    			</div>
    			<div class="top-navigation-right">
    				<div class="top-social-wrapper">
    					<div class="social-icon"><a href="#"><img src="../website-School/img/facebook.png" alt=""></a></div>
    					<div class="social-icon"><a href="#"><img src="../website-School/img/google-plus.png" alt=""></a></div>
    					<div class="social-icon"><a href="#"><img src="../website-School/img/linkedin.png" alt=""></a></div>
    					<div class="social-icon"><a href="#"><img src="../website-School/img/pinterest.png" alt=""></a></div>
    				</div>
    				<div class="top-signin">
    					<i class="fa fa-lock icon-lock"></i>
    					<a href="#" data-toggle="modal" data-target="#signinModal">Sign In</a>
    					<span class="separator">|</span>
    					<a href="#" data-toggle="modal" data-target="#signupModal">Sign Up</a>
    				</div>
    			</div>
            </div>
		</div>
	</div>
	<!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-custom">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="http://localhost/web-school/">MAGBLOG</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#">Services</a>
                    </li>
                    <li>
                        <a href="#">Portfolio</a>
                    </li>
                    <li>
                        <a href="#">About</a>
                    </li>
                    <li>
                        <a href="#">Team</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
</header>