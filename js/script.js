$(document).ready(function($) {
	$(window).scroll(function(){
		if ($(this).scrollTop() > 45) {
			$('#mainNav').addClass('navbar-fixed-top').removeClass('navbar-custom');
		} else {
			$('#mainNav').removeClass('navbar-fixed-top').addClass('navbar-custom');
		}
	});
	$("#content-slider").lightSlider({
		auto:true,
		item: 5,
        loop:true,
        slideMove:2,
        pauseOnHover: true,
        keyPress:true,
        responsive : [
            {
                breakpoint:1024,
                settings: {
                    item:4,
                    slideMove:1,
                    slideMargin:6,
                  }
            },
            {
                breakpoint: 768,
                settings:{
                    item:3,
                    slideMove:1,
                }
            },
            {
                breakpoint:640,
                settings: {
                    item:2,
                    slideMove:1,
                  }
            }
        ]
    });
    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            $(".totop").fadeIn()
        } else {
            $(".totop").fadeOut()
        }
    });
    $(".totop").on('click', function() {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return !1
    });
});